Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: whitakers-words
Source: http://mk270.github.io/whitakers-words/

Files: *
Copyright: 1993-2010 William Whitaker
           2015-2020 Martin Keegan <martin@no.ucant.org>
License: words-license
 The license.txt file distributed with the upstream source is an inaccurate
 representation of the terms that Words is actually distributed under.
 Below is an amended version based on quotes from the Notre Dame
 archives that host Whitaker's original Words program.
 (http://archives.nd.edu/whitaker/words.htm)
 .
 This is a free program, which means it is proper to copy it and pass
 it on to your friends. Consider it a developmental item for which
 there is no charge. However, just for form, it is Copyrighted
 (c). Permission is hereby freely given for any and all use of program
 and data.
 .
 This version is distributed without obligation, but the developer
 would appreciate comments and suggestions.
 .
 The source and data are freely available for anyone to use for any
 purpose. It may be converted to other languages, used in pieces, or
 modified in any way without further permission or notification.
 .
 The program source (in Ada) and dictionary are freely available for
 rehosting.

Files: debian/*
Copyright: 2020 Calum McConnell <calumlikeapplepie@gmail.com>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
