Source: whitakers-words
Section: education
Priority: optional
Maintainer: Calum McConnell <calumlikesapplepie@gmail.com>
Build-Depends:
 asciidoctor,
 debhelper-compat (=13),
 dh-ada-library (>= 8.1),
 gnat,
 gprbuild,
Standards-Version: 4.6.2
Homepage: http://mk270.github.io/whitakers-words/
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/CalumMcConnell-guest/whitakers-words
Vcs-Git: https://salsa.debian.org/CalumMcConnell-guest/whitakers-words.git

Package: whitakers-words
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, whitakers-words-data
Description: Latin word parser and translation aid
 Whitakers Words is a program that takes in words in Latin,
 and analyzes them to determine the stems, case, form,  and any
 possible  translations.  It can also provide Latin words with
 a given translation, going from Latin to English.  It is
 an invaluable tool for Latin users, be they experts or
 new learners.
 .
 Words was created by Willam Whitaker (1936-2010) as a
 curiosity that demonstrated his new programming language,
 Ada.  It was since improved and iterated on, ported away
 from it's DOS roots, and given an online access point.
 .
 This package contains the executable parser program.

Package: whitakers-words-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Latin word parser and translation aid - data
 Whitakers Words is a program that takes in words in Latin,
 and analyzes them to determine the stems, case, form,  and any
 possible  translations.  It can also provide Latin words with
 a given translation, going from Latin to English.  It is
 an invaluable tool for Latin users, be they experts or
 new learners.
 .
 Words was created by Willam Whitaker (1936-2010) as a
 curiosity that demonstrated his new programming language,
 Ada.  It was since improved and iterated on, ported away
 from is DOS roots, and given an online access point.
 .
 This package contains the Latin dictionaries.
 You should never need to install it explicitly.
